import re
from functools import partial
from types import SimpleNamespace
import argparse
import logging

logging.basicConfig(filename='scheme.log', encoding='utf-8', level=logging.DEBUG)

KEYS = dict(
OPEN = '{',
CLOSE = '}',
SEPARATOR = ':',
ESCAPE = '',
STACK = '$',
OPENESC = '(',
CLOSEESC = ')',
NEWLINEESC = ' ',
SJ = '~',
MULT = '*',
EVAL = '&',
BURN = '^',
FUNC = '@',
IF = '?',
SIEVE = '#'
)

KEYSN = SimpleNamespace(**KEYS)


def tokenize(string):
  stack = []
  root = []
  stack.append(root)
  escape = False
  for x in string:
    
    
    if x == KEYSN.OPEN and not escape:
      branch = []
      stack[-1].append(branch)
      stack.append(branch)
    elif x == KEYSN.CLOSE and not escape:
      stack.pop()
    elif x == KEYSN.SEPARATOR and not escape:
      stack[-1].append(None)
    elif x == KEYSN.ESCAPE and not escape and KEYSN.ESCAPE:
      escape = True
    else:
      stack[-1].append(x)
      escape = False
  assert len(stack) == 1
  return root


class ReplSyntaxError(AssertionError):
      pass

def representsInt(s):
  try: 
    int(s)
  except ValueError:
    return False
  else:
    return True


class LazyConstant:
  def __init__(self, data):
    self.data = data
  def __call__(self):
    return self.data
  def __repr__(self):
    return f"`{self.data}`"

class LazyPartial:
  def __init__(self, parent, arg):
    self.parent = parent
    self.arg = arg
    self.result = ...
  def __call__(self):
    if self.result is ...:
      self.parent.log("calc", self.arg)
      self.parent.depth += 1
      self.result = self.parent.interpret(self.arg)
      self.parent.depth -= 1
      self.parent.log("replace", self.result)
    else:
      self.parent.log("cached", self.result)
    return self.result
  def __repr__(self):
    return f"*{self.arg}*"

class LazyGuard:
  def __init__(self):
    self.data = []

  def add_constant(self, constant):
    self.data.append(LazyConstant(constant))

  def add(self, f):
    self.data.append(f)

  def __repr__(self):
    return '$' + repr(self.data)

  def __call__(self):
    return ''.join(x() for x in self.data)


def stringify_tokens(tokens):
  res = []
  stringmode = False
  for x in tokens:
    if isinstance(x, str) or isinstance(x, LazyConstant) :
      newstringmode = True
    else:
      newstringmode = False
    if newstringmode != stringmode:
      res.append('\'')
      stringmode = newstringmode
    if isinstance(x, str):
      if x in '{:}\\' and newstringmode:
          res.append('\\')
      if x == '\n':
          res.append('\ ')
      else:
          res.append(x.replace('\n', '\\ '))
    elif  isinstance(x, LazyConstant):
      if x.data in '{:}\\' and newstringmode:
        res.append('\\')
      res.append(x.data.replace('\n', '\\ '))
    elif isinstance(x, list):
      res.append('{')
      res.append(stringify_tokens(x))
      res.append('}')
    elif isinstance(x, LazyPartial):
      if x.result is ...:
        #res.append('(')
        res.append(stringify_tokens(x.arg))
        #res.append(')')
      else:
        res.append(stringify_tokens(x.result))
    elif isinstance(x, LazyGuard):
      res.append('[')
      res.append(stringify_tokens(x.data))
      res.append(']')
    elif x is None:
      res.append(':')
    else:
      assert False, repr(type(x))
  if stringmode:
    res.append('\'')
  return ''.join(res)
      
    


class Stack:
  verbose= True
  def __init__(self, name, *args):
    self.memstack = [list(map(str,args))]

    self.depth = 0
    self.func = {}
    self.logfile = open(name + ".log", 'w')

  @property
  def stack(self):
    return self.memstack[-1]

  def log(self, name, data):
    sdata = stringify_tokens(data) 
    if len(sdata) > 70:
          sdata = sdata[:67] + '...'
    self.logfile.write('  '*self.depth + name + ' ' +sdata + '\n')

  def parse(self,s):
    self.log('parse', s)  
    self.depth += 1
    res = [LazyGuard()]
    for x in s:
      if x is None:
        res.append(LazyGuard())
      elif isinstance(x, str):
        res[-1].add_constant(x)
      else:
        
        i = LazyPartial(self, x)
        self.log('lazy', [i])
        res[-1].add(i)
    self.depth -= 1
    self.log( 'parsed', res)
    
    return res

  def instr_stack(self, index0, index1=..., sep=KEYSN.SEPARATOR):
    if index1==...:
      index = int(index0) if index0 else 0
      try:
        return self.stack[index]
      except IndexError:
        raise ReplSyntaxError(f"cannot select argument {index} from list {self.stack}")
    else:
      index0 = int(index0) if index0 else 0
      index1 = int(index1) if index1 else None
      if index1 is None:
        return sep.join(self.stack[index0:])
      else:
        return sep.join(self.stack[index0:index1])

  def intr_mult(self, string, rep=None):
    try:
      return int(rep) * string
    except TypeError:
      return str(len(string))

  def intr_burn(self, string, fire, ash=''):
    assert fire, "invalid fire string"
    assert fire not in ash, "ash string cannot contain fire string, this will lead to an infinite loop"
    while fire in string:
      string = string.replace(fire, ash)
    return string

  #def instr_map(self, *args):
  #  return KEYSN.SEPARATOR.join(self.instr_eval('{' + a + '}') for a in args)



      
  def instr_splitjoin(self, base, splt, jn, index0=0, index1=None):
    index0 = int(index0) if index0 else 0
    index1 = int(index1) if index1 else None

    if splt:
      st = base.split(splt)
    else:
      st = base

    if index1 is None:
      return jn.join(st[index0:])
    else:
      return jn.join(st[index0:index1])

  def instr_eval(self, *code):
    code = '{' + KEYSN.SEPARATOR.join(code) + '}'
    self.log('eval', code)
    return KEYSN.SEPARATOR.join(x() for x in self.parse(tokenize(code)))

  def instr_sieve(self, old, sieve):
    return ''.join(x for x in old if x in sieve)

  def instr_if(self, sep, ontrue, onfalse):
    if sep:
      return ontrue()
    else:
      return onfalse()


  def interpret(self,token):
    
    #if self.verbose:
    #  print(' '*self.depth, 'intr', token)
    first, *args = self.parse(token)

    first = first()


    if first.startswith(KEYSN.IF):
      args = [first[1:]] + list(args)
      instr = KEYSN.IF

    elif first:
      args = [first[1:]] + [x() for x in args]
      instr = first[0]
      try:
          instr_name = list(KEYS.keys())[list(KEYS.values()).index(instr)]
      except ValueError:
          instr_name = 'invalid' + instr
      self.log('exact '+instr_name, [[x] for x in args])
    else:
      return KEYSN.SEPARATOR



    if instr == KEYSN.STACK: #stack
      return self.instr_stack(*args)

    elif instr == KEYSN.IF:
      return self.instr_if(*args)

    elif instr == KEYSN.FUNC:
          return self.exec_func(*args)
      
    elif instr == KEYSN.SIEVE:
          return self.instr_sieve(*args)

    elif instr == KEYSN.MULT:
      return self.intr_mult(*args)

    elif instr == KEYSN.BURN:
      return self.intr_burn(*args)

    elif instr == KEYSN.SJ: #splitjoin
      return self.instr_splitjoin(*args)
    elif instr == KEYSN.EVAL: #eval
      return self.instr_eval(*args)
    elif instr == KEYSN.OPENESC: #escape
      if len(args) == 1 and len(args[0]) == 0:
        return KEYSN.OPEN
      else:
        raise ReplSyntaxError("Escape instruction (OPENESC) takes no arguments")
    elif instr == KEYSN.NEWLINEESC: #escape
      if len(args) == 1 and len(args[0]) == 0:
        return '\n'
      else:
        raise ReplSyntaxError("Escape instruction (OPENESC) takes no arguments")
    elif instr == KEYSN.CLOSEESC: #escape
      if len(args) == 1 and len(args[0]) == 0:
        return KEYSN.CLOSE
      else:
        raise ReplSyntaxError("Escape instruction (CLOSEESC) takes no arguments")

    else:
      raise ReplSyntaxError(f"Invalid instruction '{instr}'")
    

  def exec_direct(self, func):
    return KEYSN.SEPARATOR.join(x() for x in self.parse(tokenize(self.func[func].strip())))
  def exec_func(self, func, *args):
    self.memstack.append(list(args))
    
    self.log(  'func '+func,[[x] for x in args])
    self.depth += 1
    result = self.exec_direct(func)
    self.depth -= 1
    self.log(  'return', [result])
    
    self.memstack.pop()
    return result

  def __call__(self, code):
    for line in code.split('\n'):
      if line:
        assert '=' in line
        a,b = line.strip().split('=', 2)
        self.func[a] = b
    return self.exec_direct("")




def exec_(code):
  return Stack()(code)
    
def verify(code, answer):
  assert Stack()(code) == answer
  print('verified', answer)
  
  
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run RRREPLACE')
    parser.add_argument('file', type=str,
                        help='path to the script')
    parser.add_argument('args', type=str, nargs='+',
                        help='The arguments of the script')
    parser.add_argument('-v', action='store_const',
                        const=True, default=False,
                        help='verbose output (debug)')
    parser.add_argument('-f', action='store',
                        type=str, default='',
                        help='read input from file')
    args = parser.parse_args()
    
    Stack.verbose = args.v
    a = args.args
    
    if args.f:
        with open(args.f) as file:
          a = list(file)
    
    with open(args.file) as file:
        print(Stack(args.file, *a)('\n'.join(file)))